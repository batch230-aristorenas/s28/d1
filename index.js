// CRUD Operations (create, Rea, Update and Delete)

/*
	- CRUD operations are the heart of any backend application
	- mastering the CRUD operations is essential for any developer
	- this helps us building character and increasing exposure to logical statements that will help us manipulate our data
	- mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information
*/

//[SECTION] Inserting Dociments (CREATE)

/*
	Syntax: 
		- db.collectionName.insertOne({object});
*/

// Insert One (inserts one docs)
db.users.insertOne({
	firstName : "Jane",
	lasrName : "Doe",
	age : 21,
	contact : {
		phone : "87654321",
		email : "janedoe@mail.com"
	},
	courses : ["CSS", "JavaScript", "Python"],
	department : "none"

})


// Insert Many (inserts one or more docs)
/*
	Syntax:
		-db.collectionName.insertMany([ {objectA}, {objectB} ])
*/

db.users.insertMany(
	[
		{
			firstName : "Stephen",
			lastName : "Hawking",
			age : 76,
			contact : {
				phone : "87000",
				email : "stephenhawking@mail.com"
			},
			courses : ["Python", "React", "PHP"],
			department : "none"
		},
		{
			firstName : "Neil",
			lastName : "Armstrong",
			age : 82,
			contact : {
				phone : "4770220",
				email : "neilarmstrong@mail.com"
			},
			courses : ["React", "Laravel", "MongoDB"],
			department : "none"
		}
	]
)


// [SECTION] Finding Docs (READ)
/*
	Syntax:
		- db.collectionName.find();
		- db.collectionName.find({field: value});
*/

//retrieves all docs
db.users.find();
// retrieves specific docs
db.users.find({firstName : "Stephen"});


// Multiple Criteria
db.users.find({lastName : "Armstrong", age : 82});


// [SECTION] Update and Replace (UPDATE)

/*
	Syntax:
		- db.collectionName.updateOne({criteria}, {set: {field/s : value}});
*/
// excercise
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});

db.users.updateOne(
	{ firstName: "Test" },
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "imrich@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
)

db.users.updateOne(
	{firstName : "Jane"},
	{
		$set :
		{
			firstName : "Jenny"
		}
	}
	)
// updating multiple docs that matches field and value /  criteria
db.users.updateMany(
	{department : "none"},
	{
		$set :
		{
			department : "HR"
		}
	}
	)

// REPLACES all the fields in a docs/object
db.users.replaceOne(
		{firstName: "Bill"}, //criteria
		{
			firstName : "Elon",
			lastName : "Musk",
			age : 30,
			courses : ["PHP", "Laravel", "HTML"],
			status : "trippings"
		}
	);
// [SECTION] Delete (Delete)
// deletes all docs that satisfies the field and value
/*
	Syntax:
		- db.collectionName.deleteOne({field : value});
		- db.collectionName.deleteMany({field : value});
*/

db.users.deleteOne({
	firstName: "Jane";
})



db.users.deleteMany({
	firstName: "Stephen";
})

db.users.deleteMany({
	firstName: "Jane";
})

db.users.deleteMany({
	department: "HR";
})






